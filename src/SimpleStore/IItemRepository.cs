﻿namespace SimpleStore
{
    public interface IItemRepository
    {
        Item GetItemByBarCode(string barCode);
    }
}
