﻿namespace SimpleStore
{
    public interface IIdGenerator
    {
        string GenerateId();
    }
}
